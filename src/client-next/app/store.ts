import { configureStore } from "@reduxjs/toolkit";
import { snack } from "../features/snack";

export const store = configureStore({
  reducer: {
    snack,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
