import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../../app/store";

export type MessageType = "error" | "success" | "info";

export type Snack = {
  message: string;
  type: MessageType;
  id: number;
};

const initialState: { messages: Snack[] } = {
  messages: [],
};

const snackSlice = createSlice({
  name: "snack",
  initialState,
  reducers: {
    showMessage(state, payload: PayloadAction<Omit<Snack, "id">>) {
      state.messages.push({ ...payload.payload, id: Date.now() });
    },

    removeMessage(state, payload: PayloadAction<number>) {
      state.messages = state.messages.filter((el) => el.id !== payload.payload);
    },
  },
});

const { removeMessage, showMessage } = snackSlice.actions;

const snackSelect = (state: RootState) => ({ messages: state.snack.messages });

export { removeMessage, showMessage, snackSelect };
export default snackSlice.reducer;
