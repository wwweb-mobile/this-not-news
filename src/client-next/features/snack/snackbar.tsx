import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { removeMessage } from '.'
import { snackSelect, MessageType } from './snackSlice'

export function Snackbar() {
  const { messages } = useSelector(snackSelect)
  const dispatch = useDispatch()

  function closeHandle(id: number) {
    return () => dispatch(removeMessage(id))
  }

  const mapStyle: Record<MessageType, string> = {
    error: 'bg-red-100 border-red-400 text-red-700',
    info: 'bg-gray-100 border-gray-400 text-gray-700',
    success: 'bg-green-100 border-green-400 text-green-700'
  }

  return (
    <div className='absolute top-4 right-4'>
      {messages.map((message) => (
        <div
          key={message.id}
          className={`border ${
            mapStyle[message.type]
          } px-4 py-3 rounded mt-2 flex w-96`}
          style={{ animation: 'opacityshow 400ms 1' }}
          role='alert'
        >
          <span className='flex-1'>{message.message}</span>
          <span className='ml-6' onClick={closeHandle(message.id)}>
            <svg
              className='fill-current h-6 w-6'
              role='button'
              xmlns='http://www.w3.org/2000/svg'
              viewBox='0 0 20 20'
            >
              <title>Закрыть</title>
              <path d='M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z' />
            </svg>
          </span>
        </div>
      ))}
    </div>
  )
}
