import type { NextApiRequest, NextApiResponse } from 'next'

type Data = {
  lastName: string
  firstName: string
}

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  res.status(200).json({ lastName: 'Doe', firstName: 'John' })
}
