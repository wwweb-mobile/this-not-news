import axios from "axios";
import type { GetServerSideProps, NextPage } from "next";
import Link from "next/link";

type Props = {
  user: {
    lastName: string;
    firstName: string;
  };
};

const Home: NextPage<Props> = ({ user }) => {
  return (
    <>
      <div>
        {user.lastName} {user.firstName}
      </div>
      <div className="mt-8">
        <Link href={"/users"}>USERS</Link>
      </div>
    </>
  );
};

//SSR
export const getServerSideProps: GetServerSideProps = async (ctx) => {
  let user;

  try {
    const response = await axios.get("http://0.0.0.0:3000/api/users");
    user = response.data;
  } catch (error) {
    console.error(error);
  }

  return {
    props: {
      user,
    },
  };
};

export default Home;
