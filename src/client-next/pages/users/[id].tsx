import axios from "axios";
import { GetStaticPaths, GetStaticProps } from "next";
import React from "react";

type Props = {
  user: {
    lastName: string;
    firstName: string;
    id: string;
  };
};

export default function UserView({ user }: Props) {
  return <div>{user.id}</div>;
}

// SSG
export const getStaticProps: GetStaticProps<any, { id: string }> = async (
  ctx
) => {
  let user;

  try {
    const response = await axios.get("http://0.0.0.0:3000/api/users");
    user = { ...response.data, id: ctx.params?.id };
  } catch (error) {
    console.error(error);
  }

  return {
    props: {
      user,
    },
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  return {
    paths: [
      { params: { id: '1' } },
      { params: { id: '2' } }
    ],
    fallback: false,
  };
};
