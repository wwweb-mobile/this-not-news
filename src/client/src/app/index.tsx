import React from 'react'
import { Routing } from 'pages'
import { hot } from 'react-hot-loader/root'
import './styles/global.scss'
import { ThemeProvider, createTheme } from '@mui/material/styles'
import { CssBaseline } from '@mui/material'
import { QueryClient, QueryClientProvider } from 'react-query'
import { theme } from './theme'
import { Provider } from 'react-redux'
import { store } from './store'
import { TranslationProvider } from 'core/i18n'

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false
    }
  }
})

export function AppComponent() {
  return (
    <Provider store={store}>
      <TranslationProvider>
        <QueryClientProvider client={queryClient}>
          <ThemeProvider theme={createTheme(theme)}>
            <CssBaseline />
            <Routing />
          </ThemeProvider>
        </QueryClientProvider>
      </TranslationProvider>
    </Provider>
  )
}

export const App = hot(AppComponent)
export { AppDispatch, RootState } from './store'
