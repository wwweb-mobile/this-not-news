import { configureStore } from '@reduxjs/toolkit'
import breadcrumb from 'features/breadcrumbs'
import profile from 'features/profile'
import locale from 'features/locale'

export const store = configureStore({
  reducer: {
    breadcrumb,
    profile,
    locale
  }
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
