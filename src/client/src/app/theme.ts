import { ThemeOptions } from '@mui/material/styles'

const theme: ThemeOptions = {
  components: {
    MuiTouchRipple: {
      defaultProps: {
        hidden: true
      }
    },
    MuiButton: {
      styleOverrides: {
        root: {
          textTransform: 'unset',
          fontSize: '13px',
          borderRadius: '8px',
          padding: '6px 20px'
        }
      }
    },
    MuiCard: {
      styleOverrides: {
        root: {
          borderRadius: '8px'
        }
      }
    },
    MuiDialog: {
      styleOverrides: {
        paper: {
          borderRadius: '8px'
        }
      }
    },
    MuiFilledInput: {
      styleOverrides: {
        root: {
          ':before': {
            display: 'none'
          },
          ':after': {
            display: 'none'
          },
          borderRadius: '8px'
        }
      }
    },
    MuiPaper: {
      styleOverrides: {
        rounded: {
          borderRadius: '8px'
        }
      }
    }
  },
  typography: {
    fontFamily: 'Roboto'
  },
  palette: {
    mode: 'light',
    primary: {
      500: '#3f6185'
    },
    background: {
      default: '#fafafa'
    }
  }
}

export { theme }
