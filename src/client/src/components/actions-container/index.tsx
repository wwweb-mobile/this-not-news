import styled from 'styled-components'

export const ActionsContainer = styled.div`
  margin: 1rem 1.5rem;
  display: flex;
  gap: 1.5rem;
`
