import styled from 'styled-components'

type Padding = 'no-top' | 'normal' | 'big'

type Props = {
  margin?: Padding
}

const marginMap: Record<Padding, string> = {
  'no-top': '0 1.5rem',
  big: '2rem 1.5rem',
  normal: '1.5rem'
}

export const CardContent = styled.div(
  ({ margin = 'no-top' }: Props) => `
  margin: ${marginMap[margin]} ;
`
)
