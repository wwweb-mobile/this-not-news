import React from 'react'
import styled from 'styled-components'

type CounterContainerProps = { link: boolean }

const CounterContainer = styled.div(
  ({ link }: CounterContainerProps) => `
  font-size: 12px;
  display: flex;
  align-items: center;
  gap: 0.5rem;
  cursor: ${link ? 'pointer' : 'default'};
  color: #878686;
  transition: all 0.2s;

  &:hover {
    ${link && 'color: #3f6185'}
  }
`
)

type Props = {
  icon: React.ReactNode
  style?: React.CSSProperties
  count?: string | number
  link?: boolean
  onClick?: () => void
}

export function Counter({ style, icon, count, link, onClick }: Props) {
  return (
    <CounterContainer style={style} link={Boolean(link)} onClick={onClick}>
      {icon} {count}
    </CounterContainer>
  )
}
