import styled from 'styled-components'

type Props = {
  url: string
}

export const ImageElement = styled.div(({ url }: Props) => `
  width: 100%;
  height: 300px;
  background-size: cover;
  background-image: url(${url});
  margin-top: 1.5rem;
`)