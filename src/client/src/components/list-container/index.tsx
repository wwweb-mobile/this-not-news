import styled from 'styled-components'

export const ListContainer = styled.div`
  display: flex;
  gap: 1.5rem;
  flex-direction: column;
`
