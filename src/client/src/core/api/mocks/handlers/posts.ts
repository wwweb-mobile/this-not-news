import { rest } from 'msw'
import { fakePosts } from '../data/fakePosts'

export const posts = [
  rest.get('/api/posts', async (req, res, ctx) => {
    return res(ctx.delay(1000), ctx.status(200), ctx.json(fakePosts))
  }),

  rest.get('/api/posts/:postId', async (req, res, ctx) => {
    const data = fakePosts.find((post) => String(post.id) === String(req.params.postId))

    if (data) return res(ctx.delay(1000), ctx.status(200), ctx.json(data))

    return res(ctx.delay(2000), ctx.status(404))
  })
]
