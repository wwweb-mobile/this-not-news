import { ProfileModel } from '../..'
import { rest } from 'msw'

const user: ProfileModel = {
  firstName: 'Илья',
  id: '1',
  lastName: 'Иванов',
  login: 'ilyaIvanov@gmail.com',
  avatarUrl: 'http://dashboards.webkom.co/react/airframe/static/41.jpg'
}

export const profile = [
  rest.post('/api/login', async (req, res, ctx) => {
    return res(ctx.delay(1000), ctx.status(200), ctx.json({ accessToken: '123' }))
  }),

  rest.get('/api/profile', async (req, res, ctx) => {
    return res(ctx.delay(200), ctx.status(200), ctx.json(user))
  })
]
