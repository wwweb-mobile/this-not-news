import { setupWorker } from 'msw'
import { posts } from './handlers/posts'
import { profile } from './handlers/profile'

export const worker = setupWorker(...posts, ...profile)
