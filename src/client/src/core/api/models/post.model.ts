export interface PostModel {
  id: number | string
  title: string
  smallText: string
  bigText: string
  imageUrl?: string 
  create_unixtime: number
  author: {
    id: number | string
    fullName: string
  }
  counters: {
    favorites: number
    comments: number
    visibilities: number
  }
}
