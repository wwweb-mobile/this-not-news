export interface ProfileModel {
  id: string 
  lastName: string
  firstName: string
  login: string
  avatarUrl: string
}