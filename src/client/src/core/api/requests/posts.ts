import axios from 'axios'
import { BaseResponse } from 'shared/types/base-response'
import { PostModel } from '..'

export type PostAPI = {
  getPosts: () => BaseResponse<PostModel[]>
  getPostById: (postId: string) => BaseResponse<PostModel>
}

export const posts: PostAPI = {
  getPosts() {
    return axios.get('/api/posts')
  },

  getPostById(postId: string) {
    return axios.get(`/api/posts/${postId}`)
  }
}
