import axios from 'axios'
import { BaseResponse } from 'shared/types/base-response'
import { ProfileModel, LoginModel } from '..'
import { getToken } from 'core/session'

export type ProfileAPI = {
  login: (login: string, password: string) => BaseResponse<LoginModel>
  getCurrentUserProfile: () => BaseResponse<ProfileModel>
}

export const profile: ProfileAPI = {
  login(login, password) {
    return axios.post('/api/login', { login, password }, { 
      headers: {
        Authorization: `Bearer ${getToken()}`
      }
    })
  },
  getCurrentUserProfile() {
    return axios.get('/api/profile')
  }
}
