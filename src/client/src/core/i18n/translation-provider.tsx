import { RootState } from 'app'
import { setLocale } from 'features/locale/localeSlice'
import React, { createContext, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { AvaibleLocales, getTranslations } from './translations'
import { Translation } from './translations/translations-type'

type TranslationContext = {
  locale: AvaibleLocales
  tr: Translation
}

export const TranslationContext = createContext<TranslationContext>({
  tr: getTranslations('enGB'),
  locale: 'enGB'
})

type Props = {
  children: React.ReactNode
}

export function TranslationProvider({ children }: Props) {
  const { locale } = useSelector((state: RootState) => ({
    locale: state.locale.locale
  }))

  const dispatch = useDispatch()

  useEffect(() => {
    const localStorageLocale = localStorage.getItem('locale') as AvaibleLocales
    const browserLocale = navigator.language as AvaibleLocales
    const locale: AvaibleLocales = localStorageLocale || browserLocale
    
    dispatch(setLocale(locale))
  }, [])

  const tr = getTranslations(locale)

  return (
    <TranslationContext.Provider value={{ locale, tr }}>{children}</TranslationContext.Provider>
  )
}
