import { Translation } from './translations-type'

export const enGB: Translation = {
  common: {
    appName: 'This is not news',
    bookmarkPosts: 'Bookmark',
    newPost: 'New post',
    newPosts: 'Latest',
    popularPosts: 'Popular'
  },
  breadcrumbs: {
    blog: 'Blog',
    home: 'Home',
    popular: 'Popular'
  },
  profile: {
    myProfile: 'Profile',
    logout: 'Logout',
    enter: 'Enter',
    password: 'Password',
    registrations: 'Registration',
    forgotYourPassword: 'Forgot your password?'
  }
}
