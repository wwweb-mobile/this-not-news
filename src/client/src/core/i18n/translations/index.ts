import { ru } from './ru'
import { enGB } from './enGB'
import { Translation } from './translations-type'

export type AvaibleLocales = 'ru' | 'enGB'

const localeMapTranslations: Record<AvaibleLocales, Translation> = {
  enGB: enGB,
  ru: ru
}

export function getTranslations(locale: AvaibleLocales) {
  return localeMapTranslations[locale] || localeMapTranslations.enGB
}
