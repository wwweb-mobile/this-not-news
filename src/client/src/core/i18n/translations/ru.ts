import { Translation } from './translations-type'

export const ru: Translation = {
  common: {
    appName: 'This is not news',
    bookmarkPosts: 'Избранное',
    newPost: 'Новая запись',
    newPosts: 'Свежее',
    popularPosts: 'Популярное'
  },
  breadcrumbs: {
    blog: 'Блог',
    home: 'Главная',
    popular: 'Популярное'
  },
  profile: {
    myProfile: 'Моя страница',
    logout: 'Выход',
    enter: 'Войти',
    password: 'Пароль',
    registrations: 'Зарегистрироваться',
    forgotYourPassword: 'Забыли пароль?'
  }
}
