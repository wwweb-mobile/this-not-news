export type Translation = {
  common: {
    appName: string
    popularPosts: string
    newPosts: string
    bookmarkPosts: string
    newPost: string
  }
  breadcrumbs: {
    home: string
    popular: string
    blog: string
  }
  profile: {
    myProfile: string
    logout: string
    enter: string
    password: string
    registrations: string
    forgotYourPassword: string
  }
}