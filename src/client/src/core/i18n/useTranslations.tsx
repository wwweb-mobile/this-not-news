import { useContext } from 'react'
import { TranslationContext } from 'core/i18n/translation-provider'

export function useTranslations() {
  const data = useContext(TranslationContext)

  return data
}
