const accessTokenProp = 'accessToken'

function getToken() {
  return localStorage.getItem(accessTokenProp)
}

function setToken(token: string) {
  return localStorage.setItem(accessTokenProp, token)
}

function removeToken() {
  return localStorage.removeItem(accessTokenProp)
}

export { getToken, setToken, removeToken }
