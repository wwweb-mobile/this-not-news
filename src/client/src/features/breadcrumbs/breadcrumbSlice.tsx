import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { RootState } from 'app'

export type Crumb = {
  linkTo: string
  title: string
}

export interface BreadcrumbsState {
  crumbs: Crumb[]
}

const initialState: BreadcrumbsState = {
  crumbs: []
}

export const counterSlice = createSlice({
  name: 'breadcrumb',
  initialState,
  reducers: {
    setBreadcrumb: (state, action: PayloadAction<Crumb[]>) => {
      state.crumbs = action.payload
    }
  }
})

export const { setBreadcrumb } = counterSlice.actions

export const crumbsSelect = (state: RootState) => ({ breadcrumbs: state.breadcrumb.crumbs })

export const breadcrumbState = counterSlice.reducer
