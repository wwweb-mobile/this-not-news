import React from 'react'
import { Breadcrumbs as MaterialBreadcrumbs, Typography } from '@mui/material'
import { Link } from 'react-router-dom'
import { useSelector } from 'react-redux'
import { crumbsSelect } from '.'

export function Breadcrumbs() {
  const { breadcrumbs } = useSelector(crumbsSelect)

  return (
    <MaterialBreadcrumbs aria-label="breadcrumb">
      {breadcrumbs.map((crumb, index, arr) => {
        if (arr.length - 1 === index) {
          return <Typography key={`${crumb.linkTo}-${index}`}>{crumb.title}</Typography>
        }
        return (
          <Link key={`${crumb.linkTo}-${index}`} to={crumb.linkTo}>
            {crumb.title}
          </Link>
        )
      })}
    </MaterialBreadcrumbs>
  )
}
