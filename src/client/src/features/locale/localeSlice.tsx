import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { RootState } from 'app'
import { AvaibleLocales } from 'core/i18n/translations'


export interface LocaleState {
  locale: AvaibleLocales
}

const initialState: LocaleState = {
  locale: 'enGB'
}

export const localeSlice = createSlice({
  name: 'breadcrumb',
  initialState,
  reducers: {
    setLocale: (state, action: PayloadAction<AvaibleLocales>) => {
      state.locale = action.payload
    }
  }
})

export function setLocale(locale: AvaibleLocales) {
  localStorage.setItem('locale', locale)
  
  return localeSlice.actions.setLocale(locale)
}

export const localeSelect = (state: RootState) => ({ locale: state.locale.locale })

export const localeState = localeSlice.reducer
