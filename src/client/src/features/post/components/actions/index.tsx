import { ActionsContainer, Counter } from 'components'
import React from 'react'
import FavoriteIcon from '@mui/icons-material/Favorite'
import CommentIcon from '@mui/icons-material/Comment'
import VisibilityIcon from '@mui/icons-material/Visibility'
import BookmarkAddIcon from '@mui/icons-material/BookmarkAdd'
import RepeatIcon from '@mui/icons-material/Repeat'

type Props = {
  favoritesCount: number
  commentsCount: number
  visibilityCount: number
  onFavoritesClick?: () => void
  onCommentsClick?: () => void
  onRepostClick?: () => void
  onBookmarkClick?: () => void
}

export function PostActions(props: Props) {
  return (
    <ActionsContainer>
      <Counter
        icon={<FavoriteIcon fontSize="small" />}
        count={props.favoritesCount}
        link
        onClick={props.onFavoritesClick}
      />
      <Counter
        icon={<CommentIcon fontSize="small" />}
        count={props.commentsCount}
        link
        onClick={props.onCommentsClick}
      />
      <Counter icon={<RepeatIcon fontSize="small" />} link onClick={props.onRepostClick} />
      <Counter icon={<BookmarkAddIcon fontSize="small" />} link onClick={props.onBookmarkClick} />

      <div style={{ flex: 1 }} />
      <Counter icon={<VisibilityIcon fontSize="small" />} count={props.visibilityCount} />
    </ActionsContainer>
  )
}
