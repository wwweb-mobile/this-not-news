import React from 'react'
import { PostModel } from 'core/api'
import { Card, Typography } from '@mui/material'
import { Link } from 'react-router-dom'
import { postView } from 'pages/nav'
import { CardContent, ActionsContainer } from 'components'
import { PostActions } from '../actions'
import { formatDate } from 'shared/utils/intl'

type Props = {
  post: PostModel
  handleBookmarkClick: () => void
  handleFavaritesClick: () => void
  handleRepostClick: () => void
}

export function PostElement({
  post,
  handleBookmarkClick,
  handleFavaritesClick,
  handleRepostClick
}: Props) {
  return (
    <Card>
      <ActionsContainer>
        <Typography variant="caption">{formatDate(post.create_unixtime, 'dateTime')}</Typography>
        {!!post.author && <Typography variant="caption">{post.author.fullName}</Typography>}
      </ActionsContainer>
      <CardContent>
        <Link to={postView(String(post.id))} style={{ color: '#000' }}>
          <Typography variant="h5">{post.title}</Typography>
        </Link>
        <Typography variant="body1" style={{ marginTop: '1rem', wordWrap: 'break-word' }}>
          {post.smallText}
        </Typography>
      </CardContent>
      <PostActions
        commentsCount={post.counters.comments}
        favoritesCount={post.counters.favorites}
        visibilityCount={post.counters.visibilities}
        onBookmarkClick={handleBookmarkClick}
        onFavoritesClick={handleFavaritesClick}
        onRepostClick={handleRepostClick}
      />
    </Card>
  )
}
