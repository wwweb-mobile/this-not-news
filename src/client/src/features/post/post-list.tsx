import React from 'react'
import { PostModel } from 'core/api'
import { ListContainer } from 'components'
import { useDispatch, useSelector } from 'react-redux'
import { openLoginForm, profileSelect } from 'features/profile'
import { PostElement } from '.'

type Props = {
  posts: PostModel[]
}

export function PostListFeature({ posts }: Props) {
  const dispatch = useDispatch()
  const { profileLoaded } = useSelector(profileSelect)

  function handleFavaritesClick() {
    if (!profileLoaded) dispatch(openLoginForm())
  }

  function handleBookmarkClick() {
    if (!profileLoaded) dispatch(openLoginForm())
  }

  function handleRepostClick() {
    if (!profileLoaded) dispatch(openLoginForm())
  }

  return (
    <ListContainer style={{ display: 'flex', gap: '1.5rem', flexDirection: 'column' }}>
      {posts.map((post) => (
        <PostElement
          key={post.id}
          post={post}
          handleBookmarkClick={handleBookmarkClick}
          handleFavaritesClick={handleFavaritesClick}
          handleRepostClick={handleRepostClick}
        />
      ))}
    </ListContainer>
  )
}
