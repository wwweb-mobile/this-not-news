import React, { useState } from 'react'
import { Dialog, TextField, Button, Typography } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import { closeLoginForm, getProfile, loginFormOpenedSelect } from '../..'
import { Link } from 'react-router-dom'
import css from './styles.scss'
import { profile as profileAPI } from 'core/api'
import { setToken } from 'core/session'
import { useTranslations } from 'core/i18n'

export function LoginDialog() {
  const { loginFormOpened } = useSelector(loginFormOpenedSelect)
  const dispatch = useDispatch()

  const [login, setLogin] = useState('')
  const [password, setPassword] = useState('')
  const [loading, setLoading] = useState(false)

  const { tr } = useTranslations()

  function handleCloseLoginForm() {
    dispatch(closeLoginForm())
  }

  async function handleLogIn(ev: React.FormEvent<HTMLFormElement>) {
    ev.preventDefault()

    try {
      setLoading(true)
      const res = await profileAPI.login(login, password)
      setToken(res.data.accessToken)
      dispatch(getProfile())
      setLogin('')
      setPassword('')
      setLoading(false)
    } catch (err) {
      console.log(err)
      setLoading(false)
    }
  }

  function handleEmailChange(ev: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) {
    setLogin(ev.target.value)
  }

  function handlePasswordChange(ev: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) {
    setPassword(ev.target.value)
  }

  return (
    <Dialog open={loginFormOpened} fullWidth onClose={handleCloseLoginForm}>
      <div className={css.container}>
        <div className={css.leftLogo}>This is not news</div>
        <div className={css.rightContainer}>
          <form onSubmit={handleLogIn}>
            <Typography variant="h5" fontWeight="bold">
              Вход в аккаунт
            </Typography>
            <div className={css.fields}>
              <TextField
                label="Email"
                value={login}
                onChange={handleEmailChange}
                variant="filled"
                size="small"
                margin="dense"
                fullWidth
                disabled={loading}
              />
              <TextField
                label={tr.profile.password}
                value={password}
                onChange={handlePasswordChange}
                variant="filled"
                size="small"
                margin="dense"
                type="password"
                fullWidth
                disabled={loading}
              />
            </div>
            <Button
              variant="contained"
              type="submit"
              fullWidth
              style={{ marginTop: '1.5rem' }}
              disabled={loading || !login || !password}
            >
              {tr.profile.enter}
            </Button>
            <div className={css.otherActions}>
              <Link to="">{tr.profile.forgotYourPassword}</Link>
              <Link to="">{tr.profile.registrations}</Link>
            </div>
          </form>
        </div>
      </div>
    </Dialog>
  )
}
