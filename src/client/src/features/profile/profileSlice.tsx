import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { RootState } from 'app'
import { ProfileModel } from 'core/api'
import { Dispatch } from '@reduxjs/toolkit'
import { profile as profileAPI } from 'core/api'
import { removeToken } from 'core/session'

interface ProfileState {
  profile: ProfileModel | null
  loaded: boolean
  loginFormOpened: boolean
}

const initialState: ProfileState = {
  profile: null,
  loaded: false,
  loginFormOpened: false
}

const profileSlice = createSlice({
  name: 'profile',
  initialState,
  reducers: {
    setUserProfile: (state, action: PayloadAction<ProfileModel>) => {
      state.profile = action.payload
      state.loaded = true
    },
    clearUserProfile: (state) => {
      state.profile = null
      state.loaded = false
    },
    openLoginForm: (state) => {
      state.loginFormOpened = true
    },
    closeLoginForm: (state) => {
      state.loginFormOpened = false
    }
  }
})

const { clearUserProfile, setUserProfile, closeLoginForm, openLoginForm } = profileSlice.actions
export const profileReducers = profileSlice.reducer

//Selects

export const loginFormOpenedSelect = (state: RootState) => ({
  loginFormOpened: state.profile.loginFormOpened
})

export const profileSelect = (state: RootState) => ({
  profileLoaded: state.profile.loaded,
  profile: state.profile.profile
})

// Actions

export { closeLoginForm, openLoginForm }

export function getProfile() {
  return async (dispatch: Dispatch, getState: () => RootState) => {
    const { profile } = getState()
    profile.loginFormOpened && dispatch(closeLoginForm())

    try {
      const res = await profileAPI.getCurrentUserProfile()
      dispatch(setUserProfile(res.data))
    } catch (err) {
      removeToken()
    }
  }
}

export function clearProfile() {
  return async (dispatch: Dispatch) => {
    removeToken()
    dispatch(clearUserProfile())
  }
}
