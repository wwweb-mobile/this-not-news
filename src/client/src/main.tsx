import React from 'react'
import * as ReactDOM from 'react-dom'
import { App } from 'app'

async function load() {
  if (process.env.NODE_ENV === 'development' && process.env.ENABLE_FAKES === 'true') {
    const mocks = await import('core/api/mocks')
    await mocks.worker.start({ onUnhandledRequest: 'bypass' })
  }

  ReactDOM.render(<App />, document.getElementById('root'))
}

load()
