import React, { useEffect } from 'react'
import { Button, Card, Typography, Tab, Tabs, Avatar } from '@mui/material'
import SubscribeIcon from '@mui/icons-material/PersonAdd'
import { TabPanel, TabContext } from '@mui/lab'
import { PostListFeature } from 'features/post'
import { fakePosts } from 'core/api/mocks/data/fakePosts'
import { Link, useRouteMatch } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { profileSelect } from 'features/profile'
import { setBreadcrumb } from 'features/breadcrumbs'
import { popularPosts, authorView } from 'pages/nav'

type AuthorTabs = 'Comments' | 'Posts'

export function AuthorView() {
  const [tab, setTab] = React.useState<AuthorTabs>('Posts')
  const dispatch = useDispatch()

  const { profile } = useSelector(profileSelect)

  const { params } = useRouteMatch<{ authorId: string }>()

  function handleChange(event: React.SyntheticEvent<Element, Event>, newValue: AuthorTabs) {
    setTab(newValue)
  }

  useEffect(() => {
    dispatch(
      setBreadcrumb([
        { linkTo: popularPosts(), title: 'Главная' },
        { linkTo: authorView(params.authorId), title: 'Иванов Илья' }
      ])
    )
  }, [])

  return (
    <TabContext value={tab}>
      <Card>
        <div style={{ padding: '1rem', display: 'flex', flexWrap: 'wrap', gap: '2rem' }}>
          <div
            style={{
              width: '180px',
              height: '180px',
              borderRadius: '8px',
              backgroundSize: 'cover',
              backgroundPosition: 'center',
              backgroundImage: 'url(http://dashboards.webkom.co/react/airframe/static/41.jpg)'
            }}
          />
          <div style={{ flex: 1 }}>
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <Typography variant="h4" style={{ flex: 1 }}>
                Илья Иванов
              </Typography>
              {Boolean(!profile || profile.id !== params.authorId) && (
                <Button variant="contained" startIcon={<SubscribeIcon />}>
                  Подписаться
                </Button>
              )}
            </div>
            <Typography variant="subtitle1">
              Блог о дизайне и разработке веб приложений. React, Next, Redux 😉
            </Typography>
            <div style={{ display: 'flex', gap: '2rem', marginTop: '1rem' }}>
              <Typography variant="body1" style={{ lineHeight: '21px' }}>
                <div style={{ fontSize: '24px', color: '#516d8a' }}>74</div>
                поста
              </Typography>
              <Typography variant="body1" style={{ lineHeight: '21px' }}>
                <div style={{ fontSize: '24px', color: '#516d8a' }}>4315</div>
                лайков
              </Typography>
              <Typography variant="body1" style={{ lineHeight: '21px' }}>
                <div style={{ fontSize: '24px', color: '#516d8a' }}>234</div>
                подписчиков
              </Typography>
            </div>
          </div>
        </div>
        <Tabs value={tab} onChange={handleChange}>
          <Tab label="Блог" value="Posts" />
          <Tab label="Комментарии" value="Comments" />
        </Tabs>
      </Card>
      <div
        style={{
          marginTop: '2rem',
          display: 'flex',
          flexWrap: 'wrap',
          gap: '1rem',
          alignItems: 'flex-start'
        }}
      >
        <div style={{ flex: 1 }}>
          <TabPanel value="Posts" style={{ padding: 0 }}>
            <PostListFeature posts={fakePosts} />
          </TabPanel>
          <TabPanel value="Comments" style={{ padding: 0 }}>
            <Card>
              <div style={{ padding: '1rem' }}>Comments</div>
            </Card>
          </TabPanel>
        </div>
        <div style={{ position: 'sticky', top: '4rem' }}>
          <Card>
            <div style={{ padding: '1rem', width: '270px' }}>
              <div style={{ display: 'flex', gap: '0.5rem', alignItems: 'center' }}>
                <Typography variant="subtitle1">Подписчики</Typography>
                <Typography variant="subtitle2">234</Typography>
              </div>
              <div style={{ margin: '1rem 0', display: 'flex', gap: '0.5rem', flexWrap: 'wrap' }}>
                {[
                  'https://gif.cmtt.space/3/user-userpic/48/40/83/d3c969ff3ca812.jpg',
                  'https://leonardo.osnova.io/86595cb9-03f9-5c57-312d-73092a0f612b/-/scale_crop/108x108/-/format/webp/',
                  'https://leonardo.osnova.io/6c681a85-69e3-b04f-6bc9-ddf6122a687f/-/scale_crop/108x108/-/format/webp/',
                  'https://leonardo.osnova.io/f049908a-f3a9-289a-d5b7-a76aa2f150ec/-/scale_crop/108x108/-/format/webp/',
                  'https://leonardo.osnova.io/c0aba387-00dd-9c62-4a1d-e31d3fc012bb/-/scale_crop/108x108/-/format/webp/',
                  'https://leonardo.osnova.io/66569a1b-73f6-5282-b5b5-5f7eab24167f/-/scale_crop/108x108/-/format/webp/',
                  'https://leonardo.osnova.io/0a009afa-cbe4-091d-0a96-01131bf86fc6/-/scale_crop/108x108/-/format/webp/',
                  'https://gif.cmtt.space/3/user-userpic/48/40/83/d3c969ff3ca812.jpg',
                  'https://leonardo.osnova.io/6c681a85-69e3-b04f-6bc9-ddf6122a687f/-/scale_crop/108x108/-/format/webp/',
                  'https://leonardo.osnova.io/f049908a-f3a9-289a-d5b7-a76aa2f150ec/-/scale_crop/108x108/-/format/webp/'
                ].map((url) => (
                  <Avatar src={url} sizes="small" />
                ))}
              </div>
              <Link to="#">Показать всех</Link>
            </div>
          </Card>
          <Card style={{ marginTop: '1rem' }}>
            <div style={{ padding: '1rem', width: '270px' }}>
              <div style={{ display: 'flex', gap: '0.5rem', alignItems: 'center' }}>
                <Typography variant="subtitle1">Подписки</Typography>
                <Typography variant="subtitle2">21</Typography>
              </div>
              <div
                style={{
                  margin: '1rem 0',
                  display: 'flex',
                  gap: '0.5rem',
                  flexWrap: 'wrap',
                  flexDirection: 'column'
                }}
              >
                {[
                  {
                    url: 'https://gif.cmtt.space/3/user-userpic/48/40/83/d3c969ff3ca812.jpg',
                    fullName: 'Yandex.Scale'
                  },
                  {
                    url: 'https://leonardo.osnova.io/86595cb9-03f9-5c57-312d-73092a0f612b/-/scale_crop/108x108/-/format/webp/',
                    fullName: 'Джон Савельев'
                  },
                  {
                    url: 'https://leonardo.osnova.io/6c681a85-69e3-b04f-6bc9-ddf6122a687f/-/scale_crop/108x108/-/format/webp/',
                    fullName: 'CSharp.dev'
                  },
                  {
                    url: 'https://leonardo.osnova.io/f049908a-f3a9-289a-d5b7-a76aa2f150ec/-/scale_crop/108x108/-/format/webp/',
                    fullName: 'Алиев Владислав'
                  }
                ].map((subscribe) => (
                  <div style={{ display: 'flex', gap: '1rem', alignItems: 'center' }}>
                    <Avatar src={subscribe.url} sizes="small" />
                    <Typography variant="body1">{subscribe.fullName}</Typography>
                  </div>
                ))}
              </div>
              <Link to="#">Показать все</Link>
            </div>
          </Card>
        </div>
      </div>
    </TabContext>
  )
}
