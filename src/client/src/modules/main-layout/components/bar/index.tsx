import React, { useState } from 'react'
import { AppBar, Avatar, Button, Container, ListItemIcon, Menu, MenuItem } from '@mui/material'
import css from './styles.scss'
import PersonIcon from '@mui/icons-material/Person'
import { useDispatch, useSelector } from 'react-redux'
import { clearProfile, openLoginForm, profileSelect } from 'features/profile'
import LogoutIcon from '@mui/icons-material/Logout'
import { useHistory } from 'react-router-dom'
import { authorView } from 'pages/nav'
import { useTranslations } from 'core/i18n'
import { setLocale } from 'features/locale/localeSlice'

const paperProp = {
  elevation: 0,
  sx: {
    overflow: 'visible',
    filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
    mt: 1.5,
    '& .MuiAvatar-root': {
      width: 32,
      height: 32,
      ml: -0.5,
      mr: 1
    },
    '&:before': {
      content: '""',
      display: 'block',
      position: 'absolute',
      top: 0,
      right: 14,
      width: 10,
      height: 10,
      bgcolor: 'background.paper',
      transform: 'translateY(-50%) rotate(45deg)',
      zIndex: 0
    }
  }
}

export function Bar() {
  const dispatch = useDispatch()
  const { profileLoaded, profile } = useSelector(profileSelect)
  const [profileDialogOpened, setProfileDialogOpened] = useState<Element | null>(null)

  const { tr } = useTranslations()

  const history = useHistory()

  function handleNewPost() {
    if (!profileLoaded) dispatch(openLoginForm())
  }

  function hadleProfileClick(ev: React.MouseEvent<HTMLDivElement, MouseEvent>) {
    if (!profileLoaded) {
      dispatch(openLoginForm())
      return
    }

    if (profileDialogOpened) {
      setProfileDialogOpened(null)
      return
    }

    setProfileDialogOpened(ev.currentTarget)
  }

  function handleClickLogout() {
    dispatch(clearProfile())
    setProfileDialogOpened(null)
  }

  function handleClickProfile() {
    setProfileDialogOpened(null)

    if (!profile) return
    history.push(authorView(profile.id))
  }

  function handleEnClick() {
    dispatch(setLocale('enGB'))
  }

  function handleRuClick() {
    dispatch(setLocale('ru'))
  }

  return (
    <AppBar position="sticky">
      <Container>
        <div className={css.barContainer}>
          <div className={css.logo}>{tr.common.appName}</div>
          <div className={css.centerBarContent}>
            <button onClick={handleRuClick}>RU</button>
            <button onClick={handleEnClick}>EN</button>
          </div>
          <div className={css.createPostContainer}>
            <Button
              variant="contained"
              onClick={handleNewPost}
              style={{ background: '#FFF', color: '#516D8A', padding: '4px 20px' }}
            >
              {tr.common.newPost}
            </Button>
          </div>
          <div className={css.personContainer} onClick={hadleProfileClick}>
            {Boolean(profile?.avatarUrl) ? (
              <Avatar
                alt={`${profile?.lastName} ${profile?.firstName}`}
                src={profile?.avatarUrl}
                style={{ width: '36px', height: '36px' }}
              />
            ) : (
              <PersonIcon color="primary" fontSize="small" />
            )}
          </div>
        </div>
      </Container>
      <Menu
        anchorEl={profileDialogOpened}
        open={!!profileDialogOpened}
        onClose={hadleProfileClick}
        onClick={hadleProfileClick}
        PaperProps={paperProp}
        transformOrigin={{ horizontal: 'right', vertical: 'top' }}
        anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
      >
        <MenuItem onClick={handleClickProfile}>
          <ListItemIcon>
            <PersonIcon fontSize="small" />
          </ListItemIcon>
          {tr.profile.myProfile}
        </MenuItem>
        <MenuItem onClick={handleClickLogout}>
          <ListItemIcon>
            <LogoutIcon fontSize="small" />
          </ListItemIcon>
          {tr.profile.logout}
        </MenuItem>
      </Menu>
    </AppBar>
  )
}
