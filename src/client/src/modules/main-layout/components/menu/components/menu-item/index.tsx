import React from 'react'
import { NavLink } from 'react-router-dom'
import styled from 'styled-components'

type Props = {
  title: string
  icon?: React.ReactNode
  linkTo: string
}

const MenuElement = styled(NavLink)`
  padding: 0.5rem 1rem;
  border-radius: 8px;
  text-decoration: none;
  display: flex;
  align-items: center;

  &.active {
    background: #fff;
    box-shadow: 0px 1px 4px 0px rgb(0 0 0 / 25%);
  }
`

const Title = styled.span`
  margin-left: 1rem;
`

export function MenuItem({ title, linkTo, icon }: Props) {
  return (
    <MenuElement to={linkTo}>
      {icon} <Title>{title}</Title>
    </MenuElement>
  )
}
