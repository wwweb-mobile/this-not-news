import React from 'react'
import { MenuItem } from './components/menu-item'
import { newPosts, popularPosts, bookmarkPosts } from 'pages/nav'
import css from './styles.scss'
import ShowChartIcon from '@mui/icons-material/ShowChart'
import AccessTimeIcon from '@mui/icons-material/AccessTime'
import BookmarkBorderIcon from '@mui/icons-material/BookmarkBorder'
import { useTranslations } from 'core/i18n'

export function Menu() {
  const { tr } = useTranslations()

  return (
    <div className={css.menuContainer}>
      <MenuItem title={tr.common.popularPosts} linkTo={popularPosts()} icon={<ShowChartIcon />} />
      <MenuItem title={tr.common.newPosts} linkTo={newPosts()} icon={<AccessTimeIcon />} />
      <MenuItem
        title={tr.common.bookmarkPosts}
        linkTo={bookmarkPosts()}
        icon={<BookmarkBorderIcon />}
      />
    </div>
  )
}
