import React from 'react'
import { Container, Grid } from '@mui/material'
import { Bar } from './components/bar'
import { Menu } from './components/menu'
import css from './styles.scss'
import { Breadcrumbs } from 'features/breadcrumbs'
import { LoginDialog } from 'features/profile'

type Props = {
  children: React.ReactNode
}

export function MainLayout({ children }: Props) {
  return (
    <>
      <Bar />
      <Container>
        <div className={css.crumbContainer}>
          <Grid container spacing={3}>
            <Grid item xs={12} md={3} lg={2}></Grid>
            <Grid item xs={12} md={9} lg={10}>
              <Breadcrumbs />
            </Grid>
          </Grid>
        </div>
        <div className={css.contentRoot}>
          <Grid container spacing={3}>
            <Grid item xs={12} md={3} lg={2}>
              <Menu />
            </Grid>
            <Grid item xs={12} md={9} lg={10}>
              {children}
            </Grid>
          </Grid>
        </div>
        <LoginDialog />
      </Container>
    </>
  )
}
