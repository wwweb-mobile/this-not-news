import { CircularProgress } from '@mui/material'
import React, { useEffect } from 'react'
import { useQuery } from 'react-query'
import { posts } from 'core/api'
import { ProgressContainer } from 'components/'
import { useDispatch } from 'react-redux'
import { setBreadcrumb } from 'features/breadcrumbs'
import { popularPosts } from 'pages/nav'
import { PostListFeature } from 'features/post'

export function PostList() {
  const { data: res, isError, isLoading } = useQuery('allPosts', posts.getPosts)

  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(
      setBreadcrumb([
        { linkTo: popularPosts(), title: 'Главная' },
        { linkTo: popularPosts(), title: 'Популярное' }
      ])
    )
  }, [])

  if (isLoading)
    return (
      <ProgressContainer>
        <CircularProgress />
      </ProgressContainer>
    )

  if (isError) return <>Произошла ошибка на сервере, попробуйте в другой раз</>

  if (!res?.data) return null

  return <PostListFeature posts={res.data} />
}
