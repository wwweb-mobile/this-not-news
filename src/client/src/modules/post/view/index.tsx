import React, { useEffect } from 'react'
import { useQuery } from 'react-query'
import { useRouteMatch } from 'react-router-dom'
import { posts } from 'core/api'
import { Card, CircularProgress, Typography } from '@mui/material'
import { ActionsContainer, CardContent, ImageElement, ProgressContainer } from 'components'
import { PostActions } from '../components/actions'
import { formatDate } from 'shared/utils/intl'
import { useDispatch } from 'react-redux'
import { setBreadcrumb } from 'features/breadcrumbs'
import { popularPosts, authorView } from 'pages/nav'

export function PostView() {
  const { params } = useRouteMatch<{ postId: string }>()
  const { postId } = params

  const dispatch = useDispatch()

  const {
    data: res,
    isLoading,
    isError
  } = useQuery(['post', postId], () => posts.getPostById(postId))

  useEffect(() => {
    if (res?.data) {
      dispatch(
        setBreadcrumb([
          { linkTo: popularPosts(), title: 'Главная' },
          { linkTo: authorView(String(res.data.author.id)), title: res.data.author.fullName },
          { linkTo: authorView(String(res.data.author.id)), title: 'Блог' }
        ])
      )
    }
  }, [res?.data])

  if (isLoading)
    return (
      <ProgressContainer>
        <CircularProgress />
      </ProgressContainer>
    )

  if (isError) return <>Произошла ошибка на сервере, попробуйте в другой раз</>

  if (!res?.data) return null

  return (
    <Card>
      <ActionsContainer>
        <Typography variant="caption">
          {formatDate(res.data.create_unixtime, 'dateTime')}
        </Typography>
        {!!res.data.author && <Typography variant="caption">{res.data.author.fullName}</Typography>}
      </ActionsContainer>
      <CardContent>
        <Typography variant="h4">{res.data.title}</Typography>
      </CardContent>
      {!!res.data.imageUrl && <ImageElement url={res.data.imageUrl} />}
      <CardContent margin="big">
        <Typography variant="body1">{res.data.bigText}</Typography>
      </CardContent>
      <PostActions
        commentsCount={res.data.counters.comments}
        favoritesCount={res.data.counters.favorites}
        visibilityCount={res.data.counters.visibilities}
      />
    </Card>
  )
}
