import React, { useEffect } from 'react'
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom'
import { popularPosts, postView, authorView } from 'pages/nav'
import { MainLayout } from 'modules/main-layout'
import { PostList, PostView } from 'modules/post'
import { AuthorView } from 'modules/author'
import { useDispatch } from 'react-redux'
import { getProfile } from 'features/profile'
import { getToken } from 'core/session'

export function Routing() {
  const dispatch = useDispatch()

  useEffect(() => {
    if (getToken()) {
      dispatch(getProfile())
    }
  }, [])

  return (
    <Router>
      <MainLayout>
        <Switch>
          <Route path={popularPosts()} exact component={PostList} />
          <Route path={postView(':postId')} exact component={PostView} />
          <Route path={authorView(':authorId')} exact component={AuthorView} />
          <Redirect to={popularPosts()} />
        </Switch>
      </MainLayout>
    </Router>
  )
}
