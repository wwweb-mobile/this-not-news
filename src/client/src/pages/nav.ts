export function popularPosts() {
  return '/popular'
}

export function bookmarkPosts() {
  return '/bookmark'
}

export function newPosts() {
  return '/new'
}

export function postView(postId: string) {
  return `/post/${postId}`
}

export function authorView(id: string) {
  return `/author/${id}`
}
