type Format = 'date' | 'dateTime' | 'time'

export function formatDate(date: Date | string | number, format: Format) {
  const options: Record<Format, Intl.DateTimeFormatOptions> = {
    date: {
      day: 'numeric',
      month: '2-digit',
      year: 'numeric'
    },
    dateTime: {
      weekday: 'long',
      day: 'numeric',
      month: '2-digit',
      hour: '2-digit',
      minute: '2-digit'
    },
    time: {
      hour: '2-digit',
      minute: '2-digit'
    }
  }

  return Intl.DateTimeFormat('ru', options[format]).format(new Date(date))
}
