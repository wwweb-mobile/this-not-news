// style loader
declare module '*.css' {
  const styles: any
  export = styles
}

declare module '*.scss' {
  const styles: any
  export = styles
}

// svg loader
declare module '*.svg' {
  const content: string
  export default content
}

type PartialPick<T, K extends keyof T> = Partial<T> & Pick<T, K>
