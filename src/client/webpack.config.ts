import { Configuration as WebpackConfiguration, EnvironmentPlugin } from 'webpack'
import { Configuration as WebpackDevServerConfiguration } from 'webpack-dev-server'
import path from 'path'
import HtmlWebpackPlugin from 'html-webpack-plugin'
import MiniCssExtractPlugin from 'mini-css-extract-plugin'
import { CleanWebpackPlugin } from 'clean-webpack-plugin'

import autoprefixer from 'autoprefixer'
import postcssNested from 'postcss-nested'
//@ts-ignore
import postcssMixins from 'postcss-mixins'
import postcssSimpleVars from 'postcss-simple-vars'
//@ts-ignore
import postcsEasings from 'postcss-easings'
import atImport from 'postcss-import'
import postcssUrl from 'postcss-url'

const isProduction = process.env.NODE_ENV === 'production'
const outPath = path.join(__dirname, '../server/Blogs.Startup/wwwroot')
const basePath = path.resolve(__dirname, 'src')

interface Configuration extends WebpackConfiguration {
  devServer?: WebpackDevServerConfiguration
}

const config: Configuration = {
  mode: isProduction ? 'production' : 'development',
  devtool: !isProduction ? 'source-map' : false,
  context: basePath,
  entry: {
    app: './main.tsx'
  },
  cache: {
    type: 'filesystem'
  },
  output: {
    path: outPath,
    publicPath: isProduction ? './' : '/',
    filename: '[name].bundle.js?[chunkhash:8]',
    chunkFilename: '[name].chank.js?[chunkhash:8]'
  },
  target: 'web',
  resolve: {
    extensions: ['.js', '.ts', '.tsx'],
    mainFields: ['module', 'browser', 'main'],
    modules: [basePath, 'node_modules'],
    alias: {
      'react-dom': '@hot-loader/react-dom'
    },
    fallback: {
      stream: false,
      http: false,
      https: false,
      zlib: false
    }
  },
  devServer: {
    historyApiFallback: true,
    static: {
      directory: path.join(__dirname, './public')
    }
  },
  module: {
    rules: [
      // .ts, .tsx
      {
        test: /\.tsx?$/,
        use: [{ loader: 'babel-loader' }, { loader: 'ts-loader' }]
      },
      // saas
      {
        test: /\.(scss|saas|css)$/,
        use: [
          isProduction ? MiniCssExtractPlugin.loader : 'style-loader',
          {
            loader: 'css-loader',
            options: {
              modules: {
                localIdentName: '[name]--[local]--[hash:base64:2]'
              },
              sourceMap: !isProduction
            }
          },
          {
            loader: 'sass-loader'
          },
          {
            loader: 'postcss-loader',
            options: {
              postcssOptions: {
                plugins: [
                  [atImport],
                  [postcssNested],
                  [postcssMixins],
                  [postcssSimpleVars],
                  [autoprefixer],
                  [postcsEasings],
                  [postcssUrl]
                ]
              }
            }
          }
        ]
      },
      // static assets
      { test: /\.html$/, use: 'html-loader' },
      { test: /\.(a?png|svg)$/, use: 'url-loader?limit=10000' },
      {
        test: /\.(jpe?g|gif|bmp|mp3|mp4|ogg|wav|eot|ttf|woff|woff2)$/,
        use: 'file-loader'
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin(),
    new EnvironmentPlugin(['NODE_ENV', 'APP_API', 'ENABLE_FAKES']),
    new MiniCssExtractPlugin({
      filename: 'style.css'
    }),
    new HtmlWebpackPlugin({
      template: '../public/index.html',
      minify: {
        minifyJS: true,
        minifyCSS: true,
        removeComments: true,
        useShortDoctype: true,
        collapseWhitespace: true,
        collapseInlineTagWhitespace: true
      },
      append: {
        head: `<script src="//cdn.polyfill.io/v3/polyfill.min.js"></script>`
      }
    })
  ]
}

export default config
