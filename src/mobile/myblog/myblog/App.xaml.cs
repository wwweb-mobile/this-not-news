﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace myblog
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new MainPage());

            //TODO:
            // ListView, работа со списками
            // FakeServices, загрузка данных
            // Вынести бизнес логику в отедльный проект, независимый от Xamarin Forms 
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
