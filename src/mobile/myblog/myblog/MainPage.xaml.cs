﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using myblog.ViewModels;
using Xamarin.Forms;

namespace myblog
{
    public partial class MainPage : ContentPage, IMainPage
    {
        public MainPage()
        {
            InitializeComponent();

            var viewModel = new MainVM(this);
            this.BindingContext = viewModel;
        }

        public void ShowListPage()
        {
            this.Navigation.PushAsync(new ListPage());
        }
    }
}
