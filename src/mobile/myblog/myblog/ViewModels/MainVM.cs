﻿using System;
using System.ComponentModel;
using System.Windows.Input;

namespace myblog.ViewModels
{
    public class MainVM : INotifyPropertyChanged
    {
        private string _myLabel;
        public string MyLabel
        {
            get
            {
                return _myLabel;
            }
            set
            {
                if (_myLabel != value)
                {
                    _myLabel = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(MyLabel)));
                }
            }
        }

        private string _myEditor;
        public string MyEditor
        {
            get
            {
                return _myEditor;
            }
            set
            {
                if (_myEditor != value)
                {
                    _myEditor = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(MyEditor)));
                }
            }
        }

        public ICommand MyButton { get; set; }

        private IMainPage _mainPage;

        public MainVM(IMainPage mainPage)
        {
            _mainPage = mainPage;
            MyLabel = "Тут будет какой-то текст";
            MyEditor = "Здесь будет поле ввода данных";

            MyButton = new Xamarin.Forms.Command(MyButtonClicked);

            this.PropertyChanged += MainVM_PropertyChanging;
        }

        private void MainVM_PropertyChanging(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(MyEditor))
            {
                MyLabel = MyEditor;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void MyButtonClicked()
        {
            _mainPage.ShowListPage();
        }
    }
}
