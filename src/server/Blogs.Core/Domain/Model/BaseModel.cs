using Ftsoft.Domain;

namespace Blogs.Core.Domain.Model;

public class BaseModel : Entity
{
    public long Id { get; set; }
}