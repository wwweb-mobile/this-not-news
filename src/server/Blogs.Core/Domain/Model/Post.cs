using Ftsoft.Domain;

namespace Blogs.Core.Domain.Model;

public class Post : BaseModel, IAggregateRoot
{
    public string Title { get; set; }
    public string Text { get; set; }
    public string ImagePath { get; set; }
    public DateTime CreateDate { get; set; }
}