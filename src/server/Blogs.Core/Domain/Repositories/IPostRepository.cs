using Blogs.Core.Domain.Model;
using Ftsoft.Storage;

namespace Blogs.Core.Domain.Repositories;

public interface IPostRepository : IRepository<Post>
{
    
}