using Blogs.Core.Domain.Model;
using Ftsoft.Domain.Specification;

namespace Blogs.Core.Domain.Specifications;

public static class PostSpecification
{
    public static ISpecification<Post> GetById(long postId) => Specification<Post>.Create(x => x.Id == postId);
    public static ISpecification<Post> GetByDate(DateTime date) => Specification<Post>.Create(x => x.CreateDate == date);
}