using Blogs.Core.Domain.Model;
using Ftsoft.Storage;
using Microsoft.EntityFrameworkCore;

namespace Blogs.Infrastructure.Database;

public class BlogContext : DbContext, IUnitOfWork
{
    public BlogContext(DbContextOptions dbContextOptions) : base(dbContextOptions)
    {
        
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfigurationsFromAssembly(typeof(BlogContext).Assembly);
        
        base.OnModelCreating(modelBuilder);
    }

    public DbSet<Post> Posts { get; set; }
}