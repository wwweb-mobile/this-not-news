using Blogs.Core.Domain.Model;
using Blogs.Core.Domain.Repositories;
using Ftsoft.Storage.EntityFramework;

namespace Blogs.Infrastructure.Database;

public sealed class PostRepository : EFRepository<Post>, IPostRepository
{
    public PostRepository(BlogContext context) : base(context)
    {
    }
}