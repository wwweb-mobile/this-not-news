using Blogs.Startup.Features.Post;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Blogs.Startup.Controllers;

public class PostController : Controller
{
    private readonly ILogger<PostController> _logger;
    private readonly IMediator _mediator;

    public PostController(ILogger<PostController> logger, IMediator mediator)
    {
        _logger = logger;
        _mediator = mediator;
    }
    
    [HttpGet]
    [Route("~/getPost")]
    public async Task<IActionResult> CreateNewPost(GetPostQuery request, CancellationToken cancellationToken)
    {
        var result = await _mediator.Send(request, cancellationToken);
        return Ok(result);
    }

    [HttpPost]
    [Route("~/createNewPost")]
    public async Task<IActionResult> CreateNewPost(CancellationToken cancellationToken)
    {
        var result = await _mediator.Send(new CreateNewPostCommand(), cancellationToken);
        return Ok();
    }

}