using Blogs.Infrastructure.Database;
using MediatR;

namespace Blogs.Startup.Features.Post;

public class CreateNewPostCommand : IRequest<bool>
{
        
}

public class CreateNewPostCommandHandler : IRequestHandler<CreateNewPostCommand, bool>
{
    private readonly BlogContext _blogContext;

    public CreateNewPostCommandHandler(BlogContext blogContext)
    {
        _blogContext = blogContext;
    }
    
    public async Task<bool> Handle(CreateNewPostCommand request, CancellationToken cancellationToken)
    {
        var post = new Core.Domain.Model.Post()
        {
            Title = "Test",
            Text = "Text2",
            ImagePath = "eee",
            CreateDate = DateTime.Now
        };

        await _blogContext.Posts.AddAsync(post, cancellationToken);
        var result = await _blogContext.SaveChangesAsync(cancellationToken);

        return result > 0;
    }
}