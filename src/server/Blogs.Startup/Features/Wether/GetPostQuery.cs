using Blogs.Core.Domain.Model;
using Blogs.Core.Domain.Repositories;
using Blogs.Core.Domain.Specifications;
using Ftsoft.Storage;
using Microsoft.AspNetCore.Mvc;

public class GetPostQuery : MediatR.IRequest<Post>
{
    [FromQuery]
    public long PostId { get; set; }
}

public class GetPostQueryHandler : MediatR.IRequestHandler<GetPostQuery, Post>
{
    private readonly IReadOnlyRepository<Post> _postRepository;

    public GetPostQueryHandler(IReadOnlyRepository<Post> postRepository)
    {
        _postRepository = postRepository;
    }
    
    public async Task<Post> Handle(GetPostQuery request, CancellationToken cancellationToken)
    {
        //Where( x => x.Id == postId && x.DateTime == DateTime.Today)
        //Where( x => x.Id == postId).Where(x => x.DateTime == DateTime.Today)
        // var specification = PostSpecification.GetById(request.PostId).And(PostSpecification.GetByDate(DateTime.Today));
        // List<Post> posts = new List<Post>();
        // var newPosts = posts.Where(specification).ToList();

        var specification = PostSpecification.GetById(request.PostId);
        var post = await _postRepository.SingleOrDefaultAsync(specification, cancellationToken);
        post.Title = "Title";
        
        await ((IRepository<Post>)_postRepository).UnitOfWork.SaveChangesAsync(cancellationToken);

        return post;
    }
}